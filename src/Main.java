import aa.dmitriev.observer.*;

public class Main {
    public static void main(String[] args) {

        Inspector inspector = new Inspector();
        Count count = new Count();
        count.addObserver(inspector);

        for (int i = 0; i < 5; i++) {
            count.countPlus();
            count.notifyObservers();
        }
    }
}