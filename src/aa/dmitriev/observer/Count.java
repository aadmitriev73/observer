package aa.dmitriev.observer;

import java.util.Observable;

public class Count extends Observable{
    private int count = 0;

    public void countPlus(){
        count++;
        super.setChanged();
    }

    public int getCount(){
        return count;
    }
}
