package aa.dmitriev.observer;

import java.util.Observable;
import java.util.Observer;

public class Inspector implements Observer{

    @Override
    public void update(Observable observable, Object o) {
        int count = ((Count) observable).getCount();
        System.out.println("Count = "+ count);
    }
}